using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Jugador
{
    public int puntaje;
    public int escobas;
    public Mazo mazojugador;
    public List<Cartas> CartasMano;
    
    public void Escobas()
    {
        puntaje += escobas;
    }
    public void SumarPunto(int ppunto)
    {
        puntaje += ppunto;
    }
    public void TengoTodosLosOros()
    {
        if (mazojugador.ORO() == 10)
        {
            this.SumarPunto(2);
        }
    }
    public void TengoLaMayoriaDeOro(Jugador pjugador)
    {
        if (this.mazojugador.ORO() > pjugador.mazojugador.ORO())
        {
            this.SumarPunto(1);
        }
        if (this.mazojugador.ORO() < pjugador.mazojugador.ORO())
        {
            pjugador.SumarPunto(1);
        }
       
    }
    public void ORO7()
    {
        foreach (Cartas item in mazojugador)
        {
            if (item.MostrarValor() == "O7" )
            {
                this.SumarPunto(1);
            }
        }
    }
    public int TodosLosSiete()
    {
        int cantsiete = 0;
        foreach (Cartas item in mazojugador)
        {
            if (item.Palo == "O" && item.Valor == 7 || item.Palo == "C" && item.Valor == 7 || item.Palo == "B" && item.Valor == 7 || item.Palo == "E" && item.Valor == 7)
            {
                cantsiete++;
            }
        }
        return cantsiete;
    }
    public void TengoMasCartas(Jugador pJugador)
    {
        if (this.mazojugador.Count() > pJugador.mazojugador.Count())
        {
            this.SumarPunto(1);
        }
        else if (this.mazojugador.Count() > pJugador.mazojugador.Count())
        {
            pJugador.SumarPunto(1);
        }
    }
}
