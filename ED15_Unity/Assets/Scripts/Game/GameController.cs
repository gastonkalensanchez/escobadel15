using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    #region Instanciar cartas a la mesa y jugadores
    public GameObject prefabcarta;
    public GameObject[] CartasMPos;
    public GameObject[] CartasJPos;
    public GameObject[] MazoJugadorPos;
    public Sprite[] caracarta;
    public Mazo Mazo;
    public static GameController instancia;

    public int mazoaux, mazojaux, mazomaux, cartasmanojaux;

    public Jugador Jugador1 = new Jugador() { mazojugador = new Mazo(), CartasMano = new List<Cartas>(), puntaje = 0 };
    public Jugador Jugador2 = new Jugador() { mazojugador = new Mazo(), CartasMano = new List<Cartas>(), puntaje = 0 };
    public List<Jugador> jugadores = new List<Jugador>();
    public List<Cartas> cartasmesa = new List<Cartas>();
    #endregion

    #region UI
    public int turno;
    public TMP_Text Turnos; //txt de los turnos
    public TMP_Text cantidadM1, cantidadM2;
    public TMP_Text puntajes;
    public TMP_Text quiengano;
    public TMP_Text puedetirar;
    public GameObject OcultarTodo;
    #endregion

    public List<string> mazojugador1 = new List<string>();
    public List<string> mazojugador2 = new List<string>();

    #region interaccion con cartas y suma 

    public GameObject slot1;
    Transform LugarSeleccionado;
    public List<GameObject> sumas = new List<GameObject>();
    public int aux = 0;
    #endregion
    public bool PuedePasarTurno;
    bool termino;
    bool puedejugar;
    public int variable;

    void Start()
    {
        if (instancia == null)
        {
            instancia = this;
        }
        puedejugar = true;
        PuedePasarTurno = true;
        jugadores.Add(Jugador1);
        jugadores.Add(Jugador2);
        Mazo = GenerarMazo();
        MezclarMazo(Mazo.mazo);
        SacarCartasMesa();
        InstanciarCartasMesa();
        puntajes.text = " ";
        quiengano.text = " ";
        puedetirar.text = " ";

    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.W))
        {
            SceneManager.LoadScene(0);
        }
        if (!termino && puedejugar)
        {
            Juego();
        }

        mazoaux = Mazo.mazo.Count;
        mazojaux = jugadores[turno].mazojugador.mazo.Count;
        cartasmanojaux = jugadores[turno].CartasMano.Count;
        mazomaux = cartasmesa.Count;

        if (termino == true)
        {
            Puntaje();
            
        }


    }
    void Juego()
    {
        Turnos.text = "Jugador" + (turno + 1).ToString();
        cantidadM1.text = "Mazo Jugador1: " + Jugador1.mazojugador.Count().ToString() + " Escobas: " + Jugador1.escobas.ToString();
        cantidadM2.text = "Mazo Jugador2: " + Jugador2.mazojugador.Count().ToString() + " Escobas: " + Jugador2.escobas.ToString();

        if (puedetirar == true)
        {
            puedetirar.text = "Si podes tirar una carta";
        }
        else
        {
            puedetirar.text = "No podes tirar una carta";
        }

   


        if (jugadores[turno].CartasMano.Count == 0)
        {
            try
            {
                SacarCartasJugador(jugadores[turno]);
                InstanciarCartasJugador(jugadores[turno]);

            }
            catch
            {
                print("No hay cartas en el mazo");
                termino = true;
                TerminarTurno();

            }

        }
        if (cartasmesa.Count == 0)
        {
            //regla escoba
            if (turno == 0)
            {
                jugadores[1].escobas++;
            }
            else if (turno == 1)
            {
                jugadores[0].escobas++;
            }
            try
            {
                SacarCartasMesa();
                InstanciarCartasMesa();

            }
            catch
            {
                print("No hay cartas en el mazo");
                termino = true;
                TerminarTurno();
            }



        }
        //si la mano del jugador no tiene nada se oculta el boton
        for (int i = 0; i < CartasJPos.Length; i++)
        {
            if (CartasJPos[i].transform.childCount == 0)
            {
                CartasJPos[i].SetActive(false);
            }
            else
            {
                CartasJPos[i].SetActive(true);
            }
        }

        SumaDelasCartas();
        Mouse();
        if (Jugador1.CartasMano.Count > 0 || Jugador2.CartasMano.Count > 0)
        {
            SumasInGame(jugadores[turno]);
        }



    }

    #region Calcular Si Hay Cartas En La Mesa
    void SumasInGame(Jugador pJugador)
    {
        for (int i = 0; i < pJugador.CartasMano.Count; i++)
        {
            SumasInGame(pJugador.CartasMano[i]);
            if (PuedePasarTurno == false)
            {
                variable = i;
                puedetirar.text = "No podes tirar una carta opcion: " + variable.ToString();
                return;
            }
        }

    }
    void SumasInGame(Cartas pCarta)
    {


        int cartamesaa = 0;
        int variablefor = 1;


        foreach (Cartas mcarta in cartasmesa.ToList())
        {
            #region cartajugador + cartamesa    

            bool sumo = false;
            int auxiliar = pCarta.Valor;
            auxiliar += mcarta.Valor;


            if (auxiliar == 15)
            {
                PuedePasarTurno = false;
                return;
            }
            if (auxiliar < 15)
            {
                //variablefor++;
                PuedePasarTurno = true;

            }
            if (auxiliar > 15)
            {
                PuedePasarTurno = true;
                continue;
            }
            #endregion

            for (int i = cartamesaa; i < cartasmesa.Count; i++)
            {

                if (mcarta.MostrarValor() == cartasmesa[i].MostrarValor())
                {
                    continue;
                }

                if (sumo && variablefor < cartasmesa.Count)
                    variablefor++;
                if (!sumo)
                    auxiliar += cartasmesa[i].Valor;



                if (auxiliar == 15)
                {
                    PuedePasarTurno = false;
                    return;
                }
                else if (auxiliar > 15)
                {
                    PuedePasarTurno = true;
                    auxiliar -= cartasmesa[i].Valor;

                    continue;
                }
                else if (auxiliar < 15)
                {
                    if (i + 1 < cartasmesa.Count)
                    {


                        if (variablefor + 1 < cartasmesa.Count)
                        {
                            auxiliar += cartasmesa[variablefor + 1].Valor; // + 3
                            //variablefor++;
                        }
                        //else
                        //{
                        //    auxiliar += cartasmesa[i].Valor;
                        //}







                        if (auxiliar > 15 && variablefor < cartasmesa.Count)
                        {
                            if (variablefor + 1 == cartasmesa.Count)
                            {
                                auxiliar -= cartasmesa[variablefor].Valor;

                                //if (sumo)
                                //    variablefor = variableforaux;

                                sumo = false;
                                continue;
                            }

                            i = cartamesaa;
                            sumo = true;
                            if (variablefor + 1 < cartasmesa.Count)
                                auxiliar -= cartasmesa[variablefor + 1].Valor;
                            continue;


                        }
                        if (auxiliar < 15 && variablefor < cartasmesa.Count)
                        {
                            PuedePasarTurno = true;
                            sumo = true;
                            i = cartamesaa;


                            //return;
                        }
                        if (auxiliar == 15 && variablefor < cartasmesa.Count)
                        {
                            PuedePasarTurno = false;
                            return;

                        }

                        //14
                    }


                    PuedePasarTurno = true;
                }



            }

            //if (PuedePasarTurno == true)
            //{
            //    return;
            //}


            cartamesaa++;
           
        }





    }
    #endregion



    public void Puntaje()
    {
        Time.timeScale = 0;
        foreach (Cartas item in cartasmesa.ToList())
        {
            jugadores[turno].mazojugador.AgregarCarta(item);
            cartasmesa.Remove(item);
        }
        foreach (Cartas item2 in Jugador1.CartasMano.ToList())
        {
            Jugador1.mazojugador.AgregarCarta(item2);
            Jugador1.mazojugador.QuitarCarta(item2);
        }
        OcultarTodo.SetActive(false);
        Jugador1.Escobas();
        Jugador2.Escobas();
        Jugador1.TengoLaMayoriaDeOro(Jugador2);
        Jugador1.ORO7();
        Jugador2.ORO7();
        Jugador1.TengoMasCartas(Jugador2);
        if (Jugador1.TodosLosSiete() == 4)
        {
            Jugador1.SumarPunto(3);
        }
        if (Jugador2.TodosLosSiete() == 4)
        {
            Jugador2.SumarPunto(3);
        }
        if (Jugador1.TodosLosSiete() > Jugador2.TodosLosSiete())
        {
            Jugador1.SumarPunto(1);
        }
        if (Jugador1.TodosLosSiete() < Jugador2.TodosLosSiete())
        {
            Jugador2.SumarPunto(1);
        }
        if (Jugador1.mazojugador.Count() < 10)
        {
            Jugador2.SumarPunto(2);
        }
        if (Jugador2.mazojugador.Count() < 10)
        {
            Jugador1.SumarPunto(2);
        }
        Turnos.text = " ";
        puntajes.text = "Jugador 1: " + Jugador1.puntaje.ToString() + " puntos" + " Jugador 2: " + Jugador2.puntaje.ToString() + " puntos";
        if (Jugador1.puntaje > Jugador2.puntaje)
        {
            quiengano.text = "Gano el jugador 1";
        }
        if (Jugador1.puntaje < Jugador2.puntaje)
        {
            quiengano.text = "Gano el jugador 2";
        }

        foreach (Cartas item1 in Jugador1.mazojugador.ToList())
        {
            mazojugador1.Add(item1.MostrarValor());
        }
        foreach (Cartas item in Jugador2.mazojugador.ToList())
        {
            mazojugador2.Add(item.MostrarValor());
        }


        puedejugar = false;
        termino = false;
        
    }

    #region interaccion con cartas y suma 
    void Mouse()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
            if (hit)
            {
                if (hit.collider.CompareTag("Carta"))
                {
                    mCarta(hit.collider.gameObject);
                    if (sumas.Count > 0)
                    {
                        BorrarSumas();
                        mCarta(hit.collider.gameObject);
                    }

                }
                if (hit.collider.CompareTag("CartaMesa"))
                {

                    CartaMesa(hit.collider.gameObject);


                }
                if (hit.collider.CompareTag("CartaMesaSola"))
                {
                    LugarSeleccionado = hit.collider.transform;
                    CartaMesaSola(LugarSeleccionado);

                }
            }
        }
    }
    void mCarta(GameObject pCarta)
    {
        print(pCarta.GetComponent<Carta>().palo + pCarta.GetComponent<Carta>().valor.ToString());
        if (slot1 == this.gameObject)
        {
            slot1 = pCarta;

        }
        else if (slot1 != pCarta)
        {
            slot1 = pCarta;

        }
    }
    void CartaMesa(GameObject CartaSeleccionada)
    {
        print("CartaMesa " + CartaSeleccionada.name);
        if (slot1 == null)
        {
            return;
        }
        else
        {

            SumaDelasCartas(CartaSeleccionada);

        }

    }
    void SumaDelasCartas()
    {
        if (aux == 15)
        {
            for (int i = 0; i < sumas.Count; i++)
            {


                sumas[i].transform.position = new Vector3(MazoJugadorPos[turno].transform.position.x, MazoJugadorPos[turno].transform.position.y, MazoJugadorPos[turno].transform.position.z - 0.02f);
                sumas[i].transform.parent = MazoJugadorPos[turno].transform;

                Cartas copiacarta = new Cartas(sumas[i].GetComponent<Carta>().palo, sumas[i].GetComponent<Carta>().valor);
                jugadores[turno].mazojugador.AgregarCarta(copiacarta);
                foreach (Cartas cartaj in jugadores[turno].CartasMano.ToList())
                {
                    if (cartaj.CartasIguales(copiacarta))
                    {
                        jugadores[turno].CartasMano.Remove(cartaj);
                    }
                }
                foreach (Cartas cartam in cartasmesa.ToList())
                {
                    if (cartam.CartasIguales(copiacarta))
                    {
                        cartasmesa.Remove(cartam);
                    }
                }


            }


            BorrarSumas();
            TerminarTurno();
            aux = 0;
        }
        if (aux > 15)
        {

            BorrarSumas();

            aux = 0;
        }
    }
    void SumaDelasCartas(GameObject pCartaSeleccionada)
    {
        foreach (GameObject item in sumas.ToArray())
        {
            if (item.name == pCartaSeleccionada.name)
            {
                return;
            }
        }
        int aux2 = 0;
        if (aux < 15)
        {
            if (sumas.Count == 0)
            {
                sumas.Add(slot1);
                aux += sumas[0].GetComponent<Carta>().valor;
            }
            for (int i = 0; i < sumas.Count; i++)
            {
                aux2 += sumas[i].GetComponent<Carta>().valor;
            }

            aux = pCartaSeleccionada.GetComponent<Carta>().valor + aux2;
            sumas.Add(pCartaSeleccionada);
        }

    }

    public void BorrarSumas()
    {
        sumas.Clear();
        slot1.GetComponent<SpriteRenderer>().color = Color.white;
        slot1 = null;
    }
    void CartaMesaSola(Transform lugar)
    {
        print("CartaMesaSola");
        if (slot1 == null)
        {
            return;
        }
        else
        {
            if (lugar.CompareTag("CartaMesaSola") && lugar.transform.childCount == 0 && sumas.Count == 0)
            {
                if (PuedePasarTurno)
                {
                    SumarAlaMesa(lugar, jugadores[turno]);
                    TerminarTurno();
                }


            }
        }

    }
    void SumarAlaMesa(Transform lugar, Jugador pjugador)
    {
        if (!slot1.CompareTag("Untagged"))//para que no se vaya el game manager
        {
            slot1.transform.parent = lugar.transform;
            slot1.transform.position = new Vector3(lugar.transform.position.x, lugar.transform.position.y, lugar.transform.position.z - 0.02f);
            lugar.tag = "CartaMesa";

            Cartas copiacarta = new Cartas(slot1.GetComponent<Carta>().palo, slot1.GetComponent<Carta>().valor);

            cartasmesa.Add(copiacarta);

            foreach (Cartas item in pjugador.CartasMano.ToList())
            {
                if (item.CartasIguales(copiacarta))
                {
                    pjugador.CartasMano.Remove(item);
                }
            }

            slot1.GetComponent<Carta>().enlamesa = true;
            slot1.GetComponent<SpriteRenderer>().color = Color.white;
            slot1 = null;
        }
    }
    #endregion

    #region Instanciar cartas a la mesa y jugadores


    void InstanciarCartasMesa()
    {
        for (int i = 0; i < cartasmesa.Count; i++)
        {

            if (CartasMPos[i].transform.childCount != 0)
            {
                Destroy(CartasMPos[i].transform.GetChild(0).gameObject);
            }

            GameObject nuevacarta = Instantiate(prefabcarta, new Vector3(CartasMPos[i].transform.position.x, CartasMPos[i].transform.position.y, CartasMPos[i].transform.position.z - 0.02f), Quaternion.identity, CartasMPos[i].transform);
            nuevacarta.name = cartasmesa[i].MostrarValor();
            nuevacarta.GetComponent<Carta>().enlamesa = true;

        }
    }
    void SacarCartasMesa()
    {
        for (int i = 0; i < 4; i++)
        {
            cartasmesa.Add(Mazo.mazo[i]);
            Mazo.QuitarCarta(Mazo.mazo[i]);
        }
    }
    void SacarCartasJugador(Jugador pJugador)
    {
        for (int i = 0; i < 3; i++)
        {
            pJugador.CartasMano.Add(Mazo.mazo[i]);
            Mazo.QuitarCarta(Mazo.mazo[i]);
        }

    }
    void InstanciarCartasJugador(Jugador pJugador)
    {
        for (int i = 0; i < 3; i++)
        {
            if (CartasJPos[i].transform.childCount != 0)
            {
                Destroy(CartasJPos[i].transform.GetChild(0).gameObject);
            }
        }

        for (int i = 0; i < pJugador.CartasMano.Count; i++)
        {


            GameObject nuevacarta = Instantiate(prefabcarta, new Vector3(CartasJPos[i].transform.position.x, CartasJPos[i].transform.position.y, CartasJPos[i].transform.position.z - 0.02f), Quaternion.identity, CartasJPos[i].transform);
            nuevacarta.name = pJugador.CartasMano[i].MostrarValor();


        }
    }

    public Mazo GenerarMazo()
    {
        Mazo pMazo = new Mazo();
        for (int i = 1; i < 11; i++)
        {
            pMazo.AgregarCarta(new Cartas("B", i));
            pMazo.AgregarCarta(new Cartas("E", i));
            pMazo.AgregarCarta(new Cartas("C", i));
            pMazo.AgregarCarta(new Cartas("O", i));
        }
        return pMazo;

    }
    public void MezclarMazo<T>(List<T> plist)
    {
        System.Random mrandom = new System.Random();
        int n = plist.Count;
        while (n > 1)
        {
            int k = mrandom.Next(n);
            n--;
            T temp = plist[k];
            plist[k] = plist[n];
            plist[n] = temp;
        }
    }

    #endregion
    public void TerminarTurno()
    {

        if (turno == 1)
        {
            if (sumas.Count > 0)
            {
                BorrarSumas();
                turno++;
            }

            turno = 0;

            InstanciarCartasJugador(jugadores[turno]);


        }
        else
        {
            if (sumas.Count > 0)
            {
                BorrarSumas();
            }
            turno++;


            InstanciarCartasJugador(jugadores[turno]);


        }
    }




}
