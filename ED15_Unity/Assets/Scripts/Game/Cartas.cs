using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cartas 
{
    public Cartas(string ppalo,int pvalor)
    {
       
        Palo = ppalo;
        Valor = pvalor;

    }
    public string Palo { get; set; }
    public int Valor { get; set; }

    public string MostrarValor()
    {
        string valor;
        valor = Palo + Valor.ToString();
        return valor;
    }
    public bool CartasIguales(Cartas pcartas2)
    {
        return (this.Palo == pcartas2.Palo && this.Valor == pcartas2.Valor);
    }

}
