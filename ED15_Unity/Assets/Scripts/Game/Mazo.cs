using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mazo : IEnumerable<Cartas>
{

    public List<Cartas> mazo = new List<Cartas>();
   
    public void AgregarCarta(Cartas pcarta)
    {
        mazo.Add(pcarta);
    }
    public void QuitarCarta(Cartas pcarta)
    {
        mazo.Remove(pcarta);
    }
    public int ORO()
    {
        int cantoro = 0;
        foreach (Cartas item in mazo)
        {
            if (item.Palo == "O")
            {
                cantoro++;
            }
        }
        return cantoro;

    }

  
    public IEnumerator<Cartas> GetEnumerator()
    {
        return mazo.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return mazo.GetEnumerator();
    }
    
  
}
