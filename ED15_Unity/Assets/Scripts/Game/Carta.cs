using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Carta : MonoBehaviour
{
    public Sprite Cara;
    private SpriteRenderer spriterenderer;

    public bool cartamesa = false;
    public string palo;
    public int valor;
    public bool enlamesa = false;
    

    

    void Start()
    {

        AsignarImagenYValor();
        spriterenderer = GetComponent<SpriteRenderer>();
   
    }

    // Update is called once per frame
    void Update()
    {
        spriterenderer.sprite = Cara;
        if (enlamesa == true)
        {      
            gameObject.tag = "CartaMesa";
        }
        PintarCartaSeleccionada();


    }


    void AsignarImagenYValor()
    {
        //genera el maso
        Mazo _mazo = GameController.instancia.GenerarMazo();

        int i = 0;
        //si el nombre de la carta es el nombre que esta en el maso se le pone su respectiva cara

        foreach (Cartas carta in _mazo)
        {
            if (this.name == carta.MostrarValor())
            {
                Cara = GameController.instancia.caracarta[i];
                palo = carta.Palo;
                valor = carta.Valor;
                break;
            }
            i++;
        }

        
    }
    void PintarCartaSeleccionada()
    {
        if (GameController.instancia.slot1)
        {
            if (name == GameController.instancia.slot1.name)
            {
                spriterenderer.color = Color.yellow;
            }
            else
            {
                spriterenderer.color = Color.white;
            }

        }
        if (GameController.instancia.sumas.Count > 0)
        {
            for (int i = 0; i < GameController.instancia.sumas.Count; i++)
            {
                if (name==GameController.instancia.sumas[i].name)
                {
                    spriterenderer.color = Color.yellow;
                }
                //else
                //{
                //    spriterenderer.color = Color.white;
                //}
            }
        }
    }
}
